#!/usr/bin/env bash
docker-compose down
cd cbioportal_mysql_data
sudo tar -czvf ../cbipportal_mysql.tgz *
docker-compose up -d
