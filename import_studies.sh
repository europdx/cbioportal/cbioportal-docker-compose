#!/usr/bin/env bash
for f in ./study/*/
do
	echo "Processing $f"
	#docker exec -it cbioportal-container metaImport.py -u http://localhost:8080 -s $f -o
        docker exec -i cbioportal-container metaImport.py -u http://localhost:8080 -s $f -o
done

#docker exec -it cbioportal-container metaImport.py -u http://localhost:8080 -s /study/lgg_ucsf_2014/ -o
docker-compose restart cbioportal
