#!/usr/bin/env bash
docker-compose down -v
#mv cbioportal_mysql_data cbioportal_mysql_data.backup-`date +"%Y-%m-%d"`
sudo rm -r cbioportal_mysql_data

# this is nice to have, can be removed or be done after the reset process
#cd cbioportal_mysql_data.backup-`date +"%Y-%m-%d"`
#sudo tar --remove-files -czvf cbipportal_mysql.tgz *
#cd ..

mkdir cbioportal_mysql_data
tar -xf cbipportal_mysql.tgz -C cbioportal_mysql_data
docker-compose up -d
